### Card game

#### clone this git repository

##### 2. composer install in card_game directory
```sh
composer install
```
##### 3. To run the program you have two
-  Run with source deck.txt file path (this file is in the root folder)
```sh
php index.php <file path>
```

or without file path
```sh
php index.php
```

#### Run the test
```sh
php vendor/bint/phpunit
```