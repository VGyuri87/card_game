<?php


namespace Controller;


class Dealer extends Player
{
    private $drawDeck = [];

    public function __construct(array $drawDeck)
    {
        $this->drawDeck = $drawDeck;
    }

    /**
     * @return string
     */
    public function deal()
    {
        $card = array_values(array_slice($this->drawDeck, -1))[0];

        unset($this->drawDeck[count($this->drawDeck)-1]);

        return $card;
    }

    /**
     * @param string $card
     */
    public function getACard($card = '')
    {
        if(!$this->isStopedTheGame()){
            if($this->getScore() < 20){
                $this->cards[] = $this->deal();
            }else{
                $this->stopTheGame();
            }
        }
    }

    /**
     * @param int $samScors
     * @return mixed
     */
    public function checkTheCards($samScors = 0)
    {
        parent::checkTheCards($samScors);

        if(($samScors > 0 && $this->getScore() <= 21) && $samScors <= $this->getScore()){
            $this->stopTheGame();
        }
        return parent::checkTheCards($samScors);
    }


    /**
     * Csak a teszteléshez kell!
     * @return array
     */
    public function getDrawDeck(){
        return $this->drawDeck;
    }
}