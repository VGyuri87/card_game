<?php


namespace Controller;


class Deck
{

    const COLOR = ['C','H','D','S'];
    const NUMBER = ['2','3','4','5','6','7','8','9','10','J','Q','K','A'];

    private $deck = [];

    /**
     * @return string
     */
    private function getCard(){
        return self::COLOR[rand(0,3)].self::NUMBER[rand(0,12)];
    }

    /**
     * @return array
     */
    public function getDeck(){
        while(count($this->deck) < 52){
            $k=-1;
            do{
                $k++;
                $card = $this->getCard();
            }while(in_array($card,$this->deck) || $k < count($this->deck));

            if($k == count($this->deck)){
                $this->deck[] = $card;
            }
        }

        return $this->deck;
    }
}