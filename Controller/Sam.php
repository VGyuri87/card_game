<?php


namespace Controller;


class Sam extends Player
{
    /**
     * @param string $card
     */
    public function getACard($card = ''){

        if(!$this->isStopedTheGame()){
            if($this->getScore() <= 17){
                $this->cards[] = $card;
            } else {
                $this->stopTheGame();
            }
        }
    }

    /**
     * @param int $samScors
     * @return mixed
     */
    public function checkTheCards($samScors = 0)
    {
       parent::checkTheCards();

       if($this->score >= 17){
            $this->stopTheGame();
       }

       return parent::checkTheCards();
    }
}