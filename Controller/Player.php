<?php


namespace Controller;


use Interfaces\Play;

abstract class Player implements Play
{
    protected $cards = [];

    protected $score;

    protected $stopedTheGame = false;

    /**
     * @param int $samScors
     * @return mixed
     */
    public function checkTheCards($samScors = 0)
    {
        if(!$this->isStopedTheGame()) {
            $this->score = 0;
            foreach ($this->cards as $card) {
                $point = $this->getCardValue($card);
                if (is_numeric($point)) {
                    $this->score += $point;
                } elseif ($point === 'A') {
                    $this->score += 11;
                } else {
                    $this->score += 10;
                }

            }
        }

        return $this->getScore();
    }

    /**
     * @return bool
     */
    public function isStopedTheGame()
    {
        return $this->stopedTheGame;
    }

    /**
     * @return int
     */
    public function getScore()
    {
        return (int)$this->score;
    }

    public function getPlayerCards(){
        return implode(" ",$this->cards);
    }

    /**
     *
     */
    protected function stopTheGame()
    {
        $this->stopedTheGame = true;
    }

    /**
     * @param $card
     * @return bool|string
     */
    private function getCardValue($card)
    {
        return substr($card,1,strlen($card)-1);
    }
}