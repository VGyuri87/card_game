<?php


use Controller\Dealer;
use Controller\Deck;
use Controller\Sam;
use PHPUnit\Framework\TestCase;

class CardGameTest extends TestCase
{
    /**
     * @var Sam
     */
    private $sam;
    private $dealer;
    private $deck;
    protected function setUp():void
    {
        parent::setUp();
        $mockDeck = $this->createMock(Deck::class);

        $mockDeck->method('getDeck')
            ->willReturn([
              "S10",
              "D5",
              "C2",
              "D8",
              "D10",
              "SJ",
              "CQ",
              "CJ",
              "D4",
              "H9",
              "S5",
              "C10",
              "S7",
              "DA",
              "S8",
              "CK",
              "D7",
              "HJ",
              "C5",
              "HK",
              "S2",
              "H5",
              "DQ",
              "C6",
              "C9",
              "H6",
              "DJ",
              "HQ",
              "C4",
              "S4",
              "C8",
              "D3",
              "H3",
              "SA",
              "H8",
              "DK",
              "S6",
              "SQ",
              "HA",
              "S9",
              "SK",
              "C7",
              "S3",
              "H2",
              "H7",
              "CA",
              "D6",
              "C3",
              "H4",
              "D2",
              "H10",
              "D9"
            ]);

        $this->deck = $mockDeck->getDeck();
        $this->dealer = new Dealer($this->deck);
        $this->sam = new Sam();
    }

    public function testDeal(){
        $this->dealer->deal();
        $this->assertEquals($this->dealer->getDrawDeck(),["S10","D5","C2","D8","D10","SJ","CQ","CJ","D4","H9","S5","C10","S7","DA","S8","CK","D7","HJ","C5","HK","S2","H5","DQ","C6","C9","H6","DJ","HQ","C4","S4","C8","D3","H3","SA","H8","DK","S6","SQ","HA","S9","SK","C7","S3","H2","H7","CA","D6","C3","H4","D2","H10"]);
    }

    public function testDealerGetACard(){
        while(!$this->dealer->isStopedTheGame()){
            $this->dealer->getACard();
            $this->dealer->checkTheCards();
        }

        $this->assertEquals(21,$this->dealer->getScore());
        $this->assertTrue($this->dealer->isStopedTheGame());
        $this->assertEquals('D9 H10 D2',$this->dealer->getPlayerCards());
    }

    public function provideDataForCardCheck(){
        return [
            [
                '7',
                '9',
                'D9',
            ],
            [
                '18',
                '19',
                'D9 H10',
            ],
            [
                '20',
                '21',
                'D9 H10 D2',
            ],
        ];
    }

    /**
     * @dataProvider provideDataForCardCheck
     */
    public function testDealerCheckTheCard($samScore, $actual,$cards){
        while(!$this->dealer->isStopedTheGame()){
            $this->dealer->getACard();
            $this->dealer->checkTheCards($samScore);
        }

        $this->assertTrue($this->dealer->isStopedTheGame());
        $this->assertEquals($actual,$this->dealer->getScore());
        $this->assertEquals($cards,$this->dealer->getPlayerCards());
    }

    public function testSamGetACardAndCheck(){
        while(!$this->sam->isStopedTheGame()){
            $this->sam->getACard($this->dealer->deal());
            $this->sam->checkTheCards();
        }

        $this->assertTrue($this->sam->isStopedTheGame());
        $this->assertEquals(19,$this->sam->getScore());
        $this->assertEquals('D9 H10',$this->sam->getPlayerCards());
    }

}