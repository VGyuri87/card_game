<?php
system('clear');
require __DIR__ . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

use Controller\Dealer;
use Controller\Deck;
use Controller\Sam;

$deck = new Deck();

if(!empty($argv[1])){
    $filePath = $argv[1];
    $fileContent = file_get_contents($filePath);
    $cards = explode(',',$fileContent);
    shuffle($cards);
    $drawDeck = $cards;
}else{
    $drawDeck = $deck->getDeck();
}

$sam = new Sam();
$dealer = new Dealer($drawDeck);

for($i = 0;$i<2;$i++){
    $sam->getACard($dealer->deal());
    $dealer->getACard();
}

$sam->checkTheCards();
$dealer->checkTheCards();

$samWin = false;
$dealerWin = false;

if($sam->getScore() == 21 && $dealer->getScore() == 21){
    $samWin = true;
} else if($sam->getScore() == 22 && $dealer->getScore() == 22){
    $dealerWin = true;
} else {
    while(!$sam->isStopedTheGame()){
        $sam->getACard($dealer->deal());
        $sam->checkTheCards();
    }

    while(!$dealer->isStopedTheGame()){
        $dealer->getACard();
        $dealer->checkTheCards($sam->getScore());
    }
    if(($sam->getScore() > $dealer->getScore() || $dealer->getScore() > 21) && $sam->getScore() <= 21){
        $samWin = true;
    }

    if(($dealer->getScore() > $sam->getScore() || $sam->getScore() > 21) && $dealer->getScore() <= 21){
        $dealerWin = true;
    }
}

if($samWin){
    echo "sam".PHP_EOL;

} else if($dealerWin){
    echo "dealer".PHP_EOL;
}

echo 'sam:    '.$sam->getPlayerCards().PHP_EOL;
echo 'dealer: '.$dealer->getPlayerCards().PHP_EOL;

